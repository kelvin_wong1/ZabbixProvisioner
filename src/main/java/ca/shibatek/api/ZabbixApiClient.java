package ca.shibatek.api;

import ca.shibatek.api.model.zabbix.ZabbixParams;
import ca.shibatek.api.model.zabbix.ZabbixRequestModel;
import ca.shibatek.api.model.zabbix.UserLogin;
import ca.shibatek.api.util.ResponseValidator;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ZabbixApiClient {

    private final ApiCaller apiCaller;

    private final String username;
    private final String password;
    private String authenticationToken;

    @Inject
    public ZabbixApiClient(@Named("zabbix.api.user.login") final String username,
                           @Named("zabbix.api.user.password") final String password,
                           final ApiCaller apiCaller) {
        this.username = username;
        this.password = password;
        this.apiCaller = apiCaller;
    }

    void authenticateClient() {
        ZabbixParams params = new UserLogin(username, password);
        ZabbixRequestModel request = getRequest(params);
        request.setMethod(ZabbixApi.USER_LOGIN.getMethod());

        JsonObject response = makeApiCall(request);
        log.info("Authenticating user: " + username);
        this.authenticationToken = ResponseValidator.getAuthToken(response);
    }

    boolean isAuthenticated() {
        if(authenticationToken != null) {
            return true;
        }
        return false;
    }

    private JsonObject makeApiCall(ZabbixRequestModel request) {
        JsonObject response = null;
        try{
            response = apiCaller.makeCall(request);
        } catch (IllegalArgumentException ex){
            ex.printStackTrace();
        }
        return response;
    }

    private ZabbixRequestModel getRequest(ZabbixParams params) {
        ZabbixRequestModel model = new ZabbixRequestModel(params);
        return model;
    }


}
