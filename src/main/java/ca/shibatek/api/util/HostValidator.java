package ca.shibatek.api.util;

import ca.shibatek.api.model.input.HostDTO;
import lombok.extern.log4j.Log4j2;

import java.util.Iterator;
import java.util.List;

@Log4j2
public class HostValidator {

    public static void removeInvalidHostDTO (List<HostDTO> hostDTOList){
        boolean remove = false;
        Iterator itr = hostDTOList.iterator();
        while(itr.hasNext()){
            HostDTO hostDTO = (HostDTO) itr.next();

            if(hostDTO.getTemplates() == null || hostDTO.getTemplates().isEmpty()){
                log.warn("Detected HostDTO with invalid templates field; object=" + hostDTO.toString());
                remove = true;
            }
            if(hostDTO.getGroups() == null || hostDTO.getGroups().isEmpty()){
                log.warn("Detected HostDTO with invalid groups field; object=" + hostDTO.toString());
                remove = true;
            }
            if(hostDTO.getInterfaces().getDns() == null && hostDTO.getInterfaces().getIp() == null ){
                log.warn("One of IP or DNS needs to be provided; object=" + hostDTO.toString());
                remove = true;
            }
            if(hostDTO.getInterfaces().getPort() == null){
                log.warn("Zabbix port needs to be provided; object=" + hostDTO.toString());
                remove = true;
            }
            if(remove){
               itr.remove();
            }
        }
    }
}
