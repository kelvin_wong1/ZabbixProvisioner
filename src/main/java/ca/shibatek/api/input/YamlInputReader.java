package ca.shibatek.api.input;

import ca.shibatek.api.model.input.HostDTO;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.error.YAMLException;

@Log4j2
public class YamlInputReader {

    public List<HostDTO> parseInputFileForHostDTO(final String inputFilePath) throws FileNotFoundException, YAMLException{
        Yaml yaml = new Yaml(new Constructor(HostDTO.class));
        List<HostDTO> hostDTOList = new ArrayList<>();
        try{
            File inputFile = new File(inputFilePath);
            InputStream fileInputStream = new FileInputStream(inputFile);
            Iterable<Object> itr = yaml.loadAll(fileInputStream);
            for(Object object : itr){
                hostDTOList.add((HostDTO) object);
            }
        } catch (FileNotFoundException ex){
            log.error("File does not exist when creating inputStream: providedPath=" + inputFilePath, ex);
            throw ex;
        } catch (YAMLException ex){
            log.error("Unexpected field in file: providedFile=" + inputFilePath, ex);
            throw ex;
        }
        return hostDTOList;
    }



}
