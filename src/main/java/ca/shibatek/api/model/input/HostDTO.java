package ca.shibatek.api.model.input;

import lombok.Data;

import java.util.List;

@Data
public class HostDTO {

    private String host;
    private HostInterfaceDTO interfaces;
    private List<String> groups;
    private List<String> templates;
}
