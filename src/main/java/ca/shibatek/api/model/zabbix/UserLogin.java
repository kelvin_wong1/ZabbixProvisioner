package ca.shibatek.api.model.zabbix;

public class UserLogin implements ZabbixParams {

    private String user;
    private String password;

    public UserLogin(final String username, final String password) {

        this.user = username;
        this.password = password;
    }
}
