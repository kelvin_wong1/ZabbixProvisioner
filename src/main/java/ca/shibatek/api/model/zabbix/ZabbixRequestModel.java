package ca.shibatek.api.model.zabbix;

public class ZabbixRequestModel {

    private final String jsonrpc = "2.0";
    private String method;
    private int id;
    private String auth;
    private final ZabbixParams params;

    public ZabbixRequestModel(final ZabbixParams params) {
        this.params = params;
    }
    public void setMethod(String method) {
        this.method = method;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuth(final String auth) {
        this.auth = auth;
    }
}
