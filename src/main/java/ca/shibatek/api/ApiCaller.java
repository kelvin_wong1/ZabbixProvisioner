package ca.shibatek.api;

import ca.shibatek.api.model.zabbix.ZabbixRequestModel;
import ca.shibatek.api.util.ResponseValidator;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Log4j2
class ApiCaller {

    private static final String ZABBIX_POST_CONTENT_TYPE = "application/json-rpc";

    private String rpcUrl;
    private final HttpClient httpClient;
    private final Gson gson;
    private final JsonParser jsonParser;
    private int requestCounter = 1;

    public ApiCaller(final HttpClient httpClient, final Gson gson, final String rpcUrl, final JsonParser jsonParser) {

        this.httpClient = httpClient;
        this.gson = gson;
        this.rpcUrl = rpcUrl;
        this.jsonParser = jsonParser;
    }

    private String sendRequest(final String requestBody) throws IllegalArgumentException{

        String response = null;
        try {
            final HttpPost request = new HttpPost(rpcUrl);
            final StringEntity params = new StringEntity(requestBody);
            request.addHeader("content-type", ZABBIX_POST_CONTENT_TYPE);
            request.setEntity(params);
            HttpResponse httpResponse = httpClient.execute(request);

            HttpEntity entity = httpResponse.getEntity();
            response = EntityUtils.toString(entity);

        } catch (UnsupportedEncodingException ex) {
            log.error("Encoding exception with POST request", ex);

        } catch (IOException ex){

        }
        return response;

    }

    public JsonObject makeCall(final ZabbixRequestModel apiRequest) {

        final String requestBody = createRequestBody(apiRequest);
        final String response = sendRequest(requestBody);

        JsonObject jsonResponse = jsonParser.parse(response).getAsJsonObject();
        checkForErrorInResponse(jsonResponse);
        return jsonResponse;
    }

    private String createRequestBody(final ZabbixRequestModel apiRequest) {

        apiRequest.setId(requestCounter);
        requestCounter++;
        final String requestBody = gson.toJson(apiRequest);

        return requestBody;
    }

    private void checkForErrorInResponse (final JsonObject jsonResponse) throws IllegalArgumentException {

        if(ResponseValidator.hasError(jsonResponse)){
            log.error("API request failed");
            throw new IllegalArgumentException("Zabbix API request failed. Response:" + jsonResponse.toString());
        }
    }


}
