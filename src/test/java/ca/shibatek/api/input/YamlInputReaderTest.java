package ca.shibatek.api.input;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

import ca.shibatek.api.model.input.HostDTO;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

public class YamlInputReaderTest {

    private static final String TEST_FILE_PATH = "add_host.yaml";
    private static final String TEST_NON_EXISTENT_PATH = "anna_kendrick.yaml";
    private static final String UNKNOWN_PROPERTY_FILE_PATH = "add_host_unknown_property.yaml";
    private String testFileAbsolutePath;
    private String testFileWithUnknownPropertyAbsolutePath;


    @Before
    public void setUp() throws Exception{
        URL resource = getClass().getClassLoader().getResource(TEST_FILE_PATH);
        File file = Paths.get(resource.toURI()).toFile();
        this.testFileAbsolutePath = file.getAbsolutePath();

        resource = getClass().getClassLoader().getResource(UNKNOWN_PROPERTY_FILE_PATH);
        file = Paths.get(resource.toURI()).toFile();
        this.testFileWithUnknownPropertyAbsolutePath = file.getAbsolutePath();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test()
    public void shouldThrowFileNotFoundExceptionIfFileDoesNotExist() throws Exception{
        exceptionRule.expect(FileNotFoundException.class);
        YamlInputReader inputReader = new YamlInputReader();
        inputReader.parseInputFileForHostDTO(TEST_NON_EXISTENT_PATH);
    }

    @Test()
    public void shouldThrowYAMLExceptionIfFileHasUnknownField() throws Exception{
        exceptionRule.expect(YAMLException.class);
        YamlInputReader inputReader = new YamlInputReader();
        inputReader.parseInputFileForHostDTO(testFileWithUnknownPropertyAbsolutePath);
    }


    @Test()
    public void shouldCreateHostDTOListWhenInputFileIsProvided() throws Exception{
        YamlInputReader inputReader = new YamlInputReader();
         List<HostDTO> hostDTOList = inputReader.parseInputFileForHostDTO(testFileAbsolutePath);

         assertFalse(hostDTOList.isEmpty());
         assertThat(hostDTOList, hasSize(2));
    }

}