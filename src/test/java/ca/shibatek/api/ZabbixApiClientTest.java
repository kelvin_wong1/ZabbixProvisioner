package ca.shibatek.api;


import ca.shibatek.api.model.zabbix.ZabbixRequestModel;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ZabbixApiClientTest {

    private final String USERNAME = "user";
    private final String PASSWORD = "password";

    @Mock
    private ApiCaller apiCaller;
    private ZabbixApiClient zabbixApiClient;
    private JsonParser parser;

    @Before
    public void setUp() {

        this.zabbixApiClient = new ZabbixApiClient(USERNAME, PASSWORD, apiCaller);
        this.parser = new JsonParser();
    }

    @Test
    public void shouldHaveAuthenticationTokenWhenSuccessfullyLoggedIn(){

        //given
        final String validReply = "{\"result\": \"0424bd59b807674191e7d77572075f33\"}";
        final JsonObject jsonResponse = parser.parse(validReply).getAsJsonObject();

        when(apiCaller.makeCall(any(ZabbixRequestModel.class))).thenReturn(jsonResponse);
        zabbixApiClient.authenticateClient();

        assertTrue(zabbixApiClient.isAuthenticated());

    }

}

